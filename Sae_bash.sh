!/bin/bash

passudo=0
sudo apt update || passudo=1

if [$passudo = 0]
then
    echo -e "Droit d'installer " 
    # Installe python
    python3.8 --version || echo  y |  sudo apt install python3.8 ||  zenity --info --title Erreur --text "python ne peut pas être installé"
    # Installe java
    java -version || echo  y |sudo apt install default-jre default-jdk|| zenity --info --title Erreur --text "java ne peut pas être installé"
    # Installe docker
    docker.io -v || echo  y | sudo apt install docker.io || zenity --info --title Erreur --text "docker.io ne peut pas être installé"
    sudo systemctl start docker
    # Installe Code
    code -v || echo  y | sudo apt install code || zenity --info --title Erreur --text "code ne peut pas être installé"
    # Installe Gitlab
    git --version | echo  y | sudo apt install git-all 
    # Installe Firfox
    firefox --version || echo y |  sudo apt install firefox ||  zenity --info --title Erreur --text "firefox ne peut pas être installé"
else
    echo -e "Pas le droit d'installer "

fi

#Configuration git
echo -e "Configuration de gitlab "

rm -r ~/.gitconfig
git config --global user.email "louis.lebeaupin@etu.univ-orleans.fr" # Rentre mon adresse mail pour git
git config --global user.name "LouisLebeaupin" # Rentre mon nom d'utilisateur git
git config --global credential.helper cache # Rentre mon mot de passe
git config --global credential.helper "cache --timeout=3600" # Enregistre mes données pendant 3600 secondes

#installation extetions  vscode
echo -e "Installation des extetions vscode "
code --install-extension ms-python.python # Extension pour avoir Python
code --install-extension njpwerner.autodocstring # Pour générer des doctstring en python
code --install-extension vscjava.vscode-java-pack # Extension pour java pour les cours de Java
code --install-extension yzhang.markdown-all-in-one # Extension markdown pour les cours Bash
code --install-extension cweijan.vscode-mysql-client2 # Extension pour sql pour la BD
code --install-extension massi.javascript-docstrings # Pour générer des doctstring en java
code --install-extension MS-vsliveshare.vsliveshare # Pour avoir notre rendu de notre code html en temps réel

exit